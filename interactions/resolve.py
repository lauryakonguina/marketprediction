from decimal import Decimal
from pytezos import pytezos

pytezos = pytezos.using(key='PRIVATE_KEY', shell='https://granadanet.smartpy.io/')
sm = pytezos.contract('BET_CONTRACT')
sm.set_bet(market_id = MARKET_ID , bet="FALSE OR TRUE").with_amount(Decimal(BET_AMOUNT)).send()