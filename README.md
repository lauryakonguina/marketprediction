# marketprediction


## Description

Structure d'un contract intelligent de prédiction de marché basique fonctionnant sur un principe de côte mutuel sur une blockchain a travers l'oracle [Harbinger](https://github.com/tacoinfra/harbinger).

## Actions

- [ ] Création d'un marché sur les assets disponibles.
- [ ] Parier sur un marché.
- [ ] Résoudre un marché et récupérer une récompense.
- [ ] Récuperer une récompense sur le paris en fonction de la côte.
